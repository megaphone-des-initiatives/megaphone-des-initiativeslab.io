
## Public website

https://initiatives-collectives.org

Repo : gitlab.com/initiatives-collectives/initiatives-collectives.gitlab.io

## Desktop browser webextension

Repo : https://gitlab.com/jibe-b/rssquater

## Mobile app (Frost fork)

Repo : https://gitlab.com/initiatives-collectives/frost/

## Page de contribution

https://contribution.initiatives-collectives.org

Repo : gitlab.com/contribution-initiatives-collectives/contribution-initiatives-collectives.gitlab.io

## Base de données 

https://baserow.io/database/19138/table/40651

## Scraper

Répo : gitlab.com/initiatives-collectives/automate-photo-reportages/

## Classification automatique (expérimentation)

Répo : https://gitlab.com/initiatives-collectives/classification
